.PHONY: clean clean-test clean-pyc clean-build help
.DEFAULT_GOAL := help

define PRINT_HELP_PYSCRIPT
import re, sys

for line in sys.stdin:
	match = re.match(r'^([a-zA-Z_-]+):.*?## (.*)$$', line)
	if match:
		target, help = match.groups()
		print("%-20s %s" % (target, help))
endef
export PRINT_HELP_PYSCRIPT


help:
	@python -c "$$PRINT_HELP_PYSCRIPT" < $(MAKEFILE_LIST)

clean: clean-build clean-pyc clean-test ## remove all build, test, coverage and Python artifacts

clean-build: ## remove build artifacts
	rm -fr build/
	rm -fr dist/
	rm -fr .eggs/
	find . \( -path ./env -o -path ./venv -o -path ./.env -o -path ./.venv \) -prune -o -name '*.egg-info' -exec rm -fr {} +
	find . \( -path ./env -o -path ./venv -o -path ./.env -o -path ./.venv \) -prune -o -name '*.egg' -exec rm -f {} +

clean-pyc: ## remove Python file artifacts
	find . -name '*.pyc' -exec rm -f {} +
	find . -name '*.pyo' -exec rm -f {} +
	find . -name '*~' -exec rm -f {} +
	find . -name '__pycache__' -exec rm -fr {} +

clean-test: ## remove test and coverage artifacts
	rm -fr .tox/
	rm -f .coverage
	rm -fr htmlcov/
	rm -fr .pytest_cache

generate-reqs: ## Generate requirements and dev for installation, requires pipenv
	python -m unittest scripts/generate_requirements.py

install-dev: ## Install Dev packages
	pipenv install --dev

install-prod: ## Install Prod packages
	pipenv install

run-dev: ## Run webserver as reload
	ENV=dev pipenv run uvicorn app:app --reload --host 0.0.0.0 --port 8070

run-prod: ## Run webserver in production
	gunicorn app:app -w 4 -k uvicorn.workers.UvicornWorker

deploy: ## Deploy to App Engine
	gcloud config set project vera-188811
	export ENV="prod";\
	gcloud app deploy app.yaml

install-frontend: ## Install npm packages for frontend
	cd ../assistant-qa-frontend && \
	npm install

build-frontend: ## Build FrontEnd and copy to static
	rm -rf static/*
	cd ../assistant-qa-frontend && \
	npm run-script build && \
	cp -r build/* ../assistant-qa-server/static/
