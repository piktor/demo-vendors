# Vera Server with Cached Results

## Installation

- Frontend

To work with front end, move to directory assisant-qa-frontend using `assisant-qa-frontend`
Install dependencies (Requires NodeJS >14) using `npm install`

To start a development server, use `npm start`

- Backend 

Install requirements with `pipenv install`
Change the virtual environment with `pipenv shell`

Spin up the server with `make run-dev`
Swagger Docs for the server are available on `/docs`

## Deployment

This project is deployed to app engine

Front End

- `cd assistant-qa-server`
- Use `make build-frontend` to create a production build
- Use `make deploy` to deploy the server changes with the new build to App engine

## Production Live Systems

`shell
mongoimport --host veradotstyle-shard-0/veradotstyle-shard-00-00.p8up2.gcp.mongodb.net:27017,veradotstyle-shard-00-01.p8up2.gcp.mongodb.net:27017,veradotstyle-shard-00-02.p8up2.gcp.mongodb.net:27017 --ssl --username ekkosign_dev --password {PASSWORD} --authenticationDatabase admin --db product-search --collection craveretail-demo --type json --jsonArray --file /home/quack/Dev/Piktorlabs/ml-experiments/craveretail/craveretail_db_simple.json
`

## Production

Deployed App 

- Type : App Engine
- Project URL - https://console.cloud.google.com/appengine/versions?serviceId=assistant-qa-server&project=vera-188811
- Deployed URL - https://assistant-qa-server-dot-vera-188811.uc.r.appspot.com/
 