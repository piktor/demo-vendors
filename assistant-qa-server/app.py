from pathlib import Path
import os

from dotenv import load_dotenv
from fastapi import FastAPI
from fastapi.middleware.cors import CORSMiddleware
from fastapi.staticfiles import StaticFiles

from config import ENVIRONMENT

if ENVIRONMENT == "prod":
    env_file = "prod.env"
else:
    env_file = "dev.env"

env_path = Path() / env_file
if env_path.exists():
    load_dotenv(env_path)


def create_app():

    app = FastAPI()
    app.add_middleware(
        CORSMiddleware,
        allow_origins=["*"],
        allow_credentials=True,
        allow_methods=["*"],
        allow_headers=["*"],
    )

    from security.auth import auth_router

    app.include_router(auth_router, tags=["auth"])

    from assistant.core import core_router

    app.include_router(core_router)

    from assistant.validate import validate_router

    app.include_router(validate_router, tags=["validation"])

    app.mount("/", StaticFiles(directory="static"), name="static")
    return app


app = create_app()

# if __name__ == "__main__":
#     import uvicorn
#     uvicorn.run(app, host="127.0.0.1", port=8000, log_level="info", access_log=True)
