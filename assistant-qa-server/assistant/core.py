import numpy as np
from clients import qa_database
from cloud_logging import cloud_logging_client
from fastapi import APIRouter, Depends, Query, status
from fastapi.responses import JSONResponse
from security.auth import get_current_active_user
from starlette.responses import FileResponse
from collections import OrderedDict

logger = cloud_logging_client.logger("cache-server")

core_router = APIRouter()

# @core_router.get("/")
# async def index_route():
#     return {"server": "running"}


@core_router.get("/")
async def read_index():
    return FileResponse("static/index.html")

@core_router.get("/{vendor}/items")
async def get_item_details(
    vendor: str,
    apparel_id: str = Query(
        None, description="list of apparel ids delimited by , (comma)"
    ),
    primary_key: str = "id",
    current_user=Depends(get_current_active_user),
):

    qa_collection = qa_database[vendor]
    apparel_ids = apparel_id.split(",")
    apparel_ids = list(OrderedDict.fromkeys(apparel_ids))

    apparel_items = list(qa_collection.find({primary_key: {"$in": apparel_ids}}))

    found_apparel_ids = [item[primary_key] for item in apparel_items] 
    apparel_ids = [aid for aid in apparel_ids if aid in found_apparel_ids]


    apparel_items = [item for item in apparel_items if item[primary_key] in apparel_ids]
    apparel_items = sorted(
        apparel_items, key=lambda x: np.where(np.asarray(apparel_ids) == x[primary_key])
    )

    exclude_fields = ["_id"]
    [[item.pop(field, None) for field in exclude_fields] for item in apparel_items]
    include_fields = [
        "id",
        "itemcode",
        "image_url",
        "department",
        "category",
        "up-sell",
        "cross-sell",
    ]
    result = []
    for item in apparel_items:
        item = {key: value for key, value in item.items() if key in include_fields}
        result.append(item)
    return JSONResponse(result)
