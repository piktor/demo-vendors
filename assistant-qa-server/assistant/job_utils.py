from datetime import datetime, timedelta

from clients import qa_database, tasks_collection
from config import QA_TIMEOUT
from pymongo import DESCENDING


def get_options(vendor):
    qa_collection = qa_database[vendor]
    # Get All valid options (service, department, category)
    unique_departments = qa_collection.distinct("department")
    unique_categories = qa_collection.distinct("category")

    result = {
        "department": unique_departments,
        "category": unique_categories,
        "vendor": vendor,
    }
    return result


def get_current_qa_jobs(vendor: str = ""):
    current_date = datetime.utcnow()
    days_before_current_date = current_date - timedelta(days=QA_TIMEOUT)
    current_jobs_query = {"start_time": {"$gte": days_before_current_date}}
    if vendor:
        current_jobs_query.update({"vendor": vendor})

    print({"current_jobs_query": current_jobs_query})
    current_qa_jobs = list(
        tasks_collection.find(current_jobs_query, sort=[("start_time", DESCENDING)])
    )

    return current_qa_jobs


def get_completed_qa_jobs(vendor=None):
    pass


def get_all_qa_vendors():
    current_date = datetime.utcnow()
    days_before_current_date = current_date - timedelta(days=QA_TIMEOUT)
    current_jobs_query = {
        "qa": {"$exists": True},
        "start_time": {"$gte": days_before_current_date},
    }

    tasks = list(tasks_collection.find(current_jobs_query))

    tasks_map = {}
    len_tasks = len(tasks)
    for i in range(len_tasks):
        current_task = tasks[len_tasks - i]
        task_data = current_task["qa"]
        task_data.update(
            {"start_time": task_data["start_time"], "task_id": task_data["task_id"]}
        )
        tasks_map.update({current_task["vendor"]: task_data})
    return tasks_map


if __name__ == "__main__":
    results = get_current_qa_jobs(vendor="uniqlo-in")
    print([res["task_id"] for res in results])
