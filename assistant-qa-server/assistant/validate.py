from clients import qa_database, tasks_collection
from fastapi import APIRouter, Depends, status, Body
from fastapi.responses import JSONResponse
from helper.db_batch import get_batch_docs, update_flag_batch
from security.auth import get_current_active_user

from assistant.job_utils import get_current_qa_jobs, get_options

validate_router = APIRouter()

validation_flag = "qa_done"
edited_flag = "qa_edited"


@validate_router.get("/{vendor}/validate/options")
async def get_validate_options(
    vendor: str, current_user=Depends(get_current_active_user)
):

    result = get_options(vendor)

    return JSONResponse(result)


@validate_router.get("/{vendor}/validate/")
async def get_items_to_validate(
    vendor: str,
    department: str = "",
    category: str = "",
    nitems: int = 20,
    current_user=Depends(get_current_active_user),
):

    qa_collection = qa_database[vendor]

    additional_query = {}
    if department:
        additional_query.update({"department": department})

    if category:
        additional_query.update({"category": category})

    docs = get_batch_docs(
        qa_collection, nitems, validation_flag, additional_query=additional_query
    )

    [doc.pop("_id") for doc in docs]
    if not docs:
        return JSONResponse(
            content={"message": "No more items available"},
            status_code=status.HTTP_404_NOT_FOUND,
        )

    include_fields = [
        "id",
        "itemcode",
        "image_url",
        "department",
        "category",
        "up-sell",
        "cross-sell",
    ]
    result = []
    for item in docs:
        item = {key: value for key, value in item.items() if key in include_fields}
        result.append(item)

    return JSONResponse(result)


@validate_router.post("/{vendor}/validate/{apparel_id}")
def validate_item_by_id(
    vendor: str,
    apparel_id: str,
    update_data: dict = Body(
        ..., embed=True, description="JSON with up-sell and cross-sell fields"
    ),
    current_user=Depends(get_current_active_user),
):

    print(update_data)
    qa_collection = qa_database[vendor]

    found_item = qa_collection.find_one({"id": apparel_id})

    if found_item is None:
        return JSONResponse(
            content={"message": "Item not found"}, status_code=status.HTTP_404_NOT_FOUND
        )

    allowed_fields = ["up-sell", "cross-sell"]
    filtered_data = {
        key: value for key, value in update_data.items() if key in allowed_fields
    }
    filtered_data.update({f"flags.{edited_flag}": True})
    print(filtered_data)
    update_result = qa_collection.update_one(
        {"id": apparel_id}, {"$set": filtered_data}
    )
    return JSONResponse({"updated": update_result.modified_count == 1})
