import os
from pymongo import MongoClient
from pymongo.errors import ConnectionFailure


def get_client(hostname=None, port=None, mongo_uri=None):
    """
    Get Client with mongouri or hostname and port
    """
    try:
        if mongo_uri:
            client = MongoClient(mongo_uri)
        else:
            client = MongoClient(hostname, port)
        return client
    except ConnectionFailure as err:
        print("Could not connect to server: %s" % err)


def collection_from_string(collection_string, client):
    """
    Get Collection from a string (db, collection) seperated by dot and a client

    Usage
    collection_from_string("vera.inventory", client)
    """
    return client[collection_string.split(".")[0]].get_collection(
        collection_string.split(".")[1]
    )


def init_mongo_client(clients_list):
    mongo_clients = {}

    if "vera-prod" in clients_list:
        mongo_clients["vera-prod"] = get_client(
            mongo_uri="mongodb+srv://{}:{}@veradotstyle.p8up2.mongodb.net/test?retryWrites=true&w=majority".format(
                os.environ.get("MONGO_VERADOTSTYLE_USERNAME"),
                os.environ.get("MONGO_VERADOTSTYLE_PASSWORD"),
            )
        )

    if "local" in clients_list:
        mongo_clients["local"] = get_client(hostname="localhost", port=27017)

    return mongo_clients


mongo_clients = init_mongo_client(["vera-prod"])
current_client = mongo_clients["vera-prod"]
db = current_client["vera_engine"]
users_collection = db.get_collection("users")

tasks_collection = db.get_collection("appengine_tasks")

qa_database = current_client["assistant_qa"]
qa_users_collection = db.get_collection("qa_users")
