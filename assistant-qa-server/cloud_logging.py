import logging

import google.cloud.logging

from config import TESTING


def setup_cloud_logging(loglevel=logging.INFO):

    # Configure logging
    if not TESTING:
        logging.basicConfig(level=loglevel)
        client = google.cloud.logging.Client()
        client.get_default_handler()

        # Attaches a Google Stackdriver logging handler to the root logger
        # client.setup_logging(loglevel)
    return client


cloud_logging_client = setup_cloud_logging()


def setup_structlog():

    import logging
    import structlog

    structlog.configure(
        processors=[
            structlog.stdlib.ProcessorFormatter.wrap_for_formatter,
        ],
        logger_factory=structlog.stdlib.LoggerFactory(),
    )

    formatter = structlog.stdlib.ProcessorFormatter(
        processor=structlog.processors.JSONRenderer(),
    )

    handler = logging.StreamHandler()
    handler.setFormatter(formatter)
    root_logger = logging.getLogger()
    root_logger.addHandler(handler)
    root_logger.setLevel(logging.INFO)

    return root_logger
