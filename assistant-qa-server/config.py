import os

# Load Env Files
from pathlib import Path
from uuid import UUID

from dotenv import load_dotenv
import os

SECRET_KEY = "d518579cf6c49800e8419b12f5ee59e7dfa3c8d8637f07309a04cd551f4a883d"
ALGORITHM = "HS256"
ACCESS_TOKEN_EXPIRE_MINUTES = 300
TESTING = False
ENVIRONMENT = os.environ.get("ENV") or "prod"
print(ENVIRONMENT)
QA_TIMEOUT = 6

env_file = f"env/{ENVIRONMENT}.env"

env_path = Path() / env_file
if env_path.exists():
    load_dotenv(env_path)
