from pipenv.project import Project
from pipenv.utils import convert_deps_to_pip


def get_reqs():
    pfile = Project(chdir=False).parsed_pipfile
    requirements = convert_deps_to_pip(pfile["packages"], r=False)
    test_requirements = convert_deps_to_pip(pfile["dev-packages"], r=False)
    return requirements, test_requirements


(requirements, test_requirements) = get_reqs()

with open("requirements.txt", "w") as rf:
    for req in requirements:
        rf.write("%s\n" % req)

with open("dev_requirements.txt", "w") as trf:
    for req in test_requirements:
        trf.write("%s\n" % req)

with open("requirements.txt", "r") as f:
    requirements = f.read().splitlines()

with open("dev_requirements.txt", "r") as f:
    dev_requirements = f.read().splitlines()

print(requirements, dev_requirements)
