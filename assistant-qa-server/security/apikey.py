from datetime import datetime
import secrets

from fastapi import APIRouter, Depends, FastAPI, HTTPException, Query, status
from fastapi.security import APIKeyHeader
from pydantic import BaseModel

from api.clients import users_collection
from api.models import User


def generate_apikey():
    import secrets

    return secrets.token_urlsafe(16)


def create_user(username, vendor):
    user_doc = {
        "vendor": vendor,
        "username": username,
        "apikey": generate_apikey(),
        "date_modified": datetime.utcnow(),
    }
    found = users_collection.find_one(user_doc)
    if not found:
        insert_result = users_collection.insert_one(user_doc)
        created = insert_result.acknowledged
        user_doc.update({"created": created})
    else:
        user_doc = found
    return user_doc


def create_new_api_key(username, vendor):
    new_apikey = generate_apikey()
    user_doc = {
        "username": username,
        "vendor": vendor,
        "apikey": new_apikey,
        "date_modified": datetime.utcnow(),
    }
    update_result = users_collection.update_one(
        {"username": username}, {"$set": user_doc}, upsert=True
    )
    updated = update_result.modified_count == 1
    user_doc.update({"updated": updated})
    return user_doc


def get_user(collection, username: str):
    user_document = collection.find_one({"username": username})
    current_user = User(**user_document)
    return current_user


X_API_KEY = APIKeyHeader(name="VERA-API-Key")


async def check_auth_header(username: str, x_api_key: str = Depends(X_API_KEY)):
    user = get_user(users_collection, username=username)
    if user.apikey != x_api_key:
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED,
            detail="Invalid API Key",
        )
    return user


"""
current_usernames = ["varun", "quack"]
current_users = []
for username in current_usernames:
    user_doc = {
        "username": username,
        "apikey": secrets.token_urlsafe(16)
    }
    current_users.append(user_doc)
current_users = [
    {'username': 'varun', 'apikey': 'VTA3nHz-fr8bUefCAKT1kg'},
    {'username': 'quack', 'apikey': 'OXlJVKBxg3Hwd_N5EHyt-A'}
]
print(current_users)
users_collection.insert_many(current_users)

app = FastAPI()


class ModelName(str, Enum):
    alexnet2 = "alexnet"
    resnet2 = "resnet"
    lenet2 = "lenet"


@app.get("/model/{model_name}")
async def get_model(
    model_name: ModelName,
    current_user=Depends(check_auth_header)
):
    if model_name == ModelName.alexnet2:
        current_response = {"model_name": model_name, "message": "Deep Learning FTW!"}

    if model_name.value == "le_net":
        current_response = {"model_name": model_name, "message": "LeCNN all the images"}

    current_response = {"model_name": model_name, "message": "Have some residuals"}

    current_response.update(current_user)
    return current_response

if __name__ == "__main__":
    import uvicorn
    uvicorn.run(app, host="127.0.0.1", port=8000, log_level="info", access_log=True)

"""

if __name__ == "__main__":
    from pprint import pprint as pr

    # create_new_api_key(username="sample-user")
    new_user = create_new_api_key(username="sample_user", vendor="sample_vendor")
    pr(new_user)
