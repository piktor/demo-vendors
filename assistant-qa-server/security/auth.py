from datetime import datetime, timedelta

import jwt
from fastapi import APIRouter, Depends, HTTPException, status
from fastapi.security import OAuth2PasswordBearer, OAuth2PasswordRequestForm
from jwt import PyJWTError
from passlib.context import CryptContext

from clients import qa_users_collection
from config import ACCESS_TOKEN_EXPIRE_MINUTES, ALGORITHM, SECRET_KEY
from security.models import RegisterUser, Token, TokenData, User, UserInDB


pwd_context = CryptContext(schemes=["bcrypt"], deprecated="auto")

oauth2_scheme = OAuth2PasswordBearer(tokenUrl="/token")

auth_router = APIRouter()


def verify_password(plain_password, hashed_password):
    return pwd_context.verify(plain_password, hashed_password)


def get_password_hash(password):
    return pwd_context.hash(password)


def get_user(collection, username: str):
    user_document = collection.find_one({"username": username})
    return UserInDB(**user_document)


def authenticate_user(collection, username: str, password: str):
    user = get_user(collection, username)
    if not user:
        return False
    if not verify_password(password, user.hashed_password):
        return False
    return user


def create_access_token(*, data: dict, expires_delta: timedelta = None):
    to_encode = data.copy()
    if expires_delta:
        expire = datetime.utcnow() + expires_delta
    else:
        expire = datetime.utcnow() + timedelta(minutes=15)
    to_encode.update({"exp": expire})
    encoded_jwt = jwt.encode(to_encode, SECRET_KEY, algorithm=ALGORITHM)
    return encoded_jwt


async def get_current_user(token: str = Depends(oauth2_scheme)):
    credentials_exception = HTTPException(
        status_code=status.HTTP_401_UNAUTHORIZED,
        detail="Could not validate credentials",
        headers={"WWW-Authenticate": "Bearer"},
    )
    try:
        payload = jwt.decode(token, SECRET_KEY, algorithms=[ALGORITHM])
        username: str = payload.get("sub")
        if username is None:
            raise credentials_exception
        token_data = TokenData(username=username)
    except PyJWTError:
        raise credentials_exception
    user = get_user(qa_users_collection, username=token_data.username)
    if user is None:
        raise credentials_exception
    return user


async def get_current_active_user(current_user: User = Depends(get_current_user)):
    if current_user.disabled:
        raise HTTPException(status_code=400, detail="Inactive user")
    return current_user


@auth_router.post("/token", response_model=Token)
async def login_for_access_token(form_data: OAuth2PasswordRequestForm = Depends()):
    user = authenticate_user(
        qa_users_collection, form_data.username, form_data.password
    )
    if not user:
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED,
            detail="Incorrect username or password",
            headers={"WWW-Authenticate": "Bearer"},
        )
    access_token_expires = timedelta(minutes=ACCESS_TOKEN_EXPIRE_MINUTES)
    access_token = create_access_token(
        data={"sub": user.username}, expires_delta=access_token_expires
    )
    return {"access_token": access_token, "token_type": "bearer"}


# @auth_router.get("/users/me/", response_model=User)
# async def read_users_me(current_user: User = Depends(get_current_active_user)):
#     return current_user


# @auth_router.get("/users/me/items/")
# async def read_own_items(current_user: User = Depends(get_current_active_user)):
#     return [{"item_id": "Foo", "owner": current_user.username}]

from clients import qa_users_collection


@auth_router.post("/register", response_model=User)
# @auth_router.post("/register", response_model=User, include_in_schema=False)
async def register_user_details(user_data: RegisterUser):
    user = user_data
    hashed_password = get_password_hash(user.password)
    user_document = UserInDB(
        **user.dict(exclude={"password"}), **{"hashed_password": hashed_password}
    )
    insert_result = qa_users_collection.insert_one(user_document.dict())
    if not insert_result.acknowledged:
        HTTPException(
            detail="Register failed", status_code=status.HTTP_500_INTERNAL_SERVER_ERROR
        )
    return User(**user_document.dict())


@auth_router.post("/admin/activate")
# @auth_router.post("/admin/activate", include_in_schema=False)
async def change_user_status(
    username: str, disabled: bool, current_user=Depends(get_current_active_user)
):

    if not current_user.admin:
        HTTPException(
            detail="Not enough permissions to access route",
            status=status.HTTP_403_FORBIDDEN,
        )

    user_result = qa_users_collection.update_one(
        {"username": username}, {"$set": {"disabled": disabled}}
    )

    acknowledgement = user_result.matched_count or user_result.modified_count

    return acknowledgement
