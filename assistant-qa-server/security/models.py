from pydantic import BaseModel
from enum import Enum


class Token(BaseModel):
    access_token: str
    token_type: str


class TokenData(BaseModel):
    username: str = None


class User(BaseModel):
    username: str
    email: str = None
    full_name: str = None
    disabled: bool = True
    admin: bool = False


class RegisterUser(BaseModel):
    username: str
    password: str
    email: str = None
    full_name: str = None


class UserInDB(User):
    hashed_password: str


class VendorName(str, Enum):
    craveretail_demo = "craveretail-demo"
